<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\User;

class UserController extends Controller
{
    public function personalAction()
    {
        if (Core::getInstance()->requestMethod === 'POST') {
            $v = Core::getInstance()->v;
            $v->name('first_name')->value($_POST['first_name'])->type('text')->length(3);
            $v->name('last_name')->value($_POST['last_name'])->type('text')->length(3);
            $v->name('address')->value($_POST['address'])->type('text')->length(3);
            $v->name('phone')->value($_POST['last_name'])->type('text')->length(3, 9);
            if ($v->isSuccess()) {
                $row = $v->getData();
                User::addInfo($row);
            }
            else {
                $errors = $v->getDisplayErrors();
                $model = $v->getData();
                return $this->indexAction($this->render(null, [
                    'model' => $model,
                    'errors' => $errors
                ]), [
                ]);
            }
        }
        $userInfo = User::getInfo();

        return $this->indexAction($this->render(null, [
            'user' => $userInfo
        ]), [
            'user' => $userInfo
        ]);
    }

    public function indexAction($content)
    {
        if (!User::isUserAuthorize())
            return $this->redirect('/user/login');
        $userInfo = User::getInfo();
        return $this->render('views/user/index.php', [
            'content' => $content,
            'user' => $userInfo
        ]);
    }

    public function registerAction()
    {
        if (User::isUserAuthorize())
            $this->redirect('/');
        if (Core::getInstance()->requestMethod === 'POST') {
            $v = Core::getInstance()->v;
            $v->name('email')->value($_POST['email'])->type('email')->required();

            $v->name('password')->value($_POST['password'])->type('text')->length(6, 100)->required();
            $v->name('repassword')->value($_POST['repassword'])->type('text')->equal($_POST['password'])->required();

            if (!$v->isSuccess()) {
                $errors = $v->getDisplayErrors();
                $model = $v->getData();
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model
                ]);

            } else {
                User::setUser($_POST['email'], $_POST['password']);
                return $this->redirect("/user/login");
            }

        } else
            return $this->render();
    }

    public function loginAction()
    {

        if (User::isUserAuthorize())
            $this->redirect('/');
        if (Core::getInstance()->requestMethod === 'POST') {
            $v = Core::getInstance()->v;
            $v->name('email')->value($_POST['email'])->type('email')->required();
            $v->name('password')->value($_POST['password'])->type('text')->required();
            if (!$v->isSuccess()) {
                $errors = $v->getDisplayErrors();
                return $this->render(null, [
                    'errors' => $errors
                ]);
            }

            $user = User::authenticateUser($_POST['email'], $_POST['password']);
            $error = null;
            if (empty($user)) {
                $error = 'Неправильний логін або пароль';
            } else {
                User::authorizeUser($user);
                $this->redirect('/');
            }
        }
        return $this->render(null, [
            'error' => $error ?? ''
        ]);
    }

    public function logoutAction()
    {
        User::logoutUser();
        $this->redirect('/user/login');
    }

    private function validation()
    {

    }

}