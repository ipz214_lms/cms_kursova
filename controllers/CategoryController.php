<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Category;
use models\Product;
use models\User;

class CategoryController extends Controller
{
    protected $errors = [];

    public function indexAction()
    {
        $rows = Category::getCategories();
        $viewPath = null;
        if (User::isAdmin())
            $viewPath = "views/category/index-admin.php";
        return $this->render($viewPath,
            [
                'rows' => $rows
            ]);
    }

    public function viewAction($params) {
        if(!is_numeric($params[0]) || (int) $params[0] <= 0) {
            return $this->error('404');
        }
        $id = (int)($params[0]);
        $category = Category::getCategoryById($id);
        $products = Product::getProductsByCategoryId($id);
        return $this->render(null, [
            'category' => $category,
            'products' => $products
        ]);
    }
    public function addAction()
    {
        if (!User::isAdmin())
            return $this->error(403);
        if (Core::getInstance()->requestMethod === 'POST') {
            $v = Core::getInstance()->v;
            $v->name('name')->value($_POST['name'])->type('text')->length(3, 50)->required();
            $v->name('file')->file($_FILES['file'])->ext(['png', 'jpg'])->maxSize(2097152);

            if (!$v->isSuccess()) {
                $model = $v->getData();
                $errors = $v->getDisplayErrors();
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model
                ]);
            }
            Category::addCategory($_POST['name'], $_FILES['file']['tmp_name']);
            return $this->redirect('/category/index');
        }
        return $this->render();
    }

    public function deleteAction($params)
    {
        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $category = Category::getCategoryById($id);
            if ($yes) {
                $filePath = 'files/category/' . $category['photo'];
                if (is_file($filePath))
                    unlink($filePath);
                Category::deleteCategory($id);
                return $this->redirect('/category/index');
            }
            return $this->render(null, [
                'category' => $category
            ]);
        } else
            return $this->error(403);
    }

    public function editAction($params)
    {
        $id = intval($params[0]);
        if (!User::isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $category = Category::getCategoryById($id);
            if (Core::getInstance()->requestMethod === 'POST') {
                $v = Core::getInstance()->v;
                $v->name('name')->value($_POST['name'])->type('text')->length(3, 50)->required();
                $v->name('file')->file($_FILES['file'])->ext(['png', 'jpg'])->maxSize(2097152);

                if (!$v->isSuccess()) {
                    $model = $v->getData();
                    $errors = $v->getDisplayErrors();
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'category' => $category
                    ]);
                }

                Category::updateCategory($id, $_POST['name']);
                if (!empty($_FILES['file']['tmp_name']))
                    Category::changePhoto($id, $_FILES['file']['tmp_name']);
                return $this->redirect('/category/index');
            }

            return $this->render(null, [
                'category' => $category
            ]);
        } else
            return $this->error(403);
    }


}