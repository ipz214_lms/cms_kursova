<?php

namespace controllers;

use core\Utils;
use models\Cart;
use models\Product;

class CartController extends \core\Controller
{
    public function indexAction() {
        $products = Cart::getItems();

        return $this->render(null, [
            'products' => $products ?? []
        ]);
    }
    public function addAction($params) {
        if(!Utils::isValidId($params[0])) {
            return $this->error('404');
        }
        $id = (int) $params[0];
        $c = $params[1] ?? 1;
        Cart::addItem($id, $c);
    }
    public function deleteAction($params) {
        if(!Utils::isValidId($params[0])) {
            return $this->error('404');
        }
        $id = (int) $params[0];
        Cart::deleteItem($id);
        $this->redirect('/cart');
    }
}