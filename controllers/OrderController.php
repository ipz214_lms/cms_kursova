<?php

namespace controllers;

use core\Core;
use models\Customer;
use models\Order;
use models\User;

class OrderController extends \core\Controller
{
    public function indexAction()
    {

    }

    public function addAction()
    {
        if (empty($_SESSION)) {
            return $this->error('403', 'Нема замовлень ');
        }
        if (User::getCurrentUserId() !== false) {
            $userInfo = User::getInfo();
        }
        if (Core::getInstance()->requestMethod == 'POST') {
            $v = Core::getInstance()->v;
            $v->name('first_name')->value($_POST['first_name'])->type('text')->length(3);
            $v->name('last_name')->value($_POST['last_name'])->type('text')->length(3);
            $v->name('address')->value($_POST['address'])->type('text')->length(3);
            $v->name('phone')->value($_POST['last_name'])->type('text')->length(3, 9);
            if ($v->isSuccess()) {
                $row = $v->getData();
                $id = Customer::addCustomer($row);
                Order::makeOrder($id);
            }
            else {
                $errors = $v->getDisplayErrors();
                $model = $v->getData();
                return $this->render(null,
                [
                    'errors' => $errors,
                    'model' => $model
                ]);
            }

            } else {
                $model = $userInfo ?? [];
                return $this->render(null, [
                    'model' => $model
                ]);
            }
        }
    }