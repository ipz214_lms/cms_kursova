<?php

namespace controllers;

use core\Utils;
use models\User;
use models\Wish;

class WishController extends \core\Controller
{
    public function indexAction()
    {
        if (!User::isUserAuthorize()) {
            $this->redirect('/user/login');
        }

        $user = User::getCurrentUserId();
        $products = Wish::getItemsDB($user) ?? [];

        return $this->render(null, [
            'products' => $products
        ]);
    }

    public function addAction($params)
    {
        if (!Utils::isValidId($params[0])) {
            return $this->error('404');
        }

        if (!User::isUserAuthorize()) {
            $this->redirect('/user/login');
        }

        $id = (int)$params[0];
        $user = User::getCurrentUserId();
        Wish::addDB($user, $id);

// TODO: redirect
    }

    public function removeAction($params)
    {
        if (!Utils::isValidId($params[0])) {
            return $this->error('404');
        }

        if (!User::isUserAuthorize()) {
            $this->redirect('/user/login');
        }

        $id = (int)$params[0];
        $user = User::getCurrentUserId();
        Wish::removeDB($user, $id);
        $this->redirect('/wish');
    }

}