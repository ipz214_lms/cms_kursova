<?php

namespace controllers;

use core\Controller;
use core\Core;
use core\Utils;
use models\Category;
use models\Product;
use models\User;

class ProductController extends Controller
{
    public function indexAction()
    {
        return $this->render();
    }

    public function addAction($params)
    {
        if (!User::isAdmin())
            return $this->error(403);
        $categories = Category::getCategories();
        $category_id = intval($params[0]) ?? null;
        if (Core::getInstance()->requestMethod == 'POST') {
            $v = self::validation($categories);
            if (!$v->isSuccess()) {
                $model = $v->getData();
                $errors = $v->getDisplayErrors();
                return $this->render(null, [
                    'model' => $model,
                    'errors' => $errors,
                    'categories' => $categories,
                    'category_id' => $category_id
                ]);
            }
            $row = $_POST;
            Product::addProduct($row, $_FILES['file']['tmp_name']);
            $this->redirect('/product');
        }
        return $this->render(null, [
            'categories' => $categories,
            'category_id' => $category_id
        ]);
    }

    public function editAction($params)
    {
        if (!User::isAdmin())
            return $this->error(403);

        if (!Utils::isValidId($params[0])) {
            return $this->error(404);
        }

        $id = intval($params[0]);
        $product = Product::getProductById($id);
        if (is_null($product)) {
            return $this->error(404);
        }

        $categories = Category::getCategories();

        $model = $product;
        $categories = $categories ?? [];
        $errors = null;
        if (Core::getInstance()->requestMethod === 'POST') {
            $v = self::validation($categories);
            if (!$v->isSuccess()) {
                $errors = $v->getDisplayErrors();
                $model = $v->getData();
            } else {
                $row = $v->getData();
                Product::updateProduct($id, $row);
                if (!empty($_FILES['file']['tmp_name']))
                    Product::changePhoto($id, $_FILES['file']['tmp_name']);
                $this->redirect('/product/index');
            }
        }

        // pass data to view
        return $this->render(null, [
            'model' => $model,
            'categories' => $categories,
            'errors' => $errors
        ]);
    }

    private function validation($categories)
    {
        $v = Core::getInstance()->v;
        $max_id = end($categories)['category_id'];
        $min_id = reset($categories)['category_id'];

        $v->name('name')->value($_POST['name'])->type('text')->length(3, 50)->required();
        $v->name('category_id')->value($_POST['category_id'])->type('int')->length($min_id, $max_id)->required();
        $v->name('count')->value($_POST['count'])->type('int')->length(0)->required();
        $v->name('price')->value($_POST['price'])->type('number')->length(0)->required();
        $v->name('description')->value($_POST['description'])->type('text');
        $v->name('file')->file($_FILES['file'])->ext(['png', 'jpg'])->maxSize(2097152);
        return $v;
    }

    public function deleteAction($params)
    {
        if (!User::isAdmin())
            return $this->error(403);
        if (!Utils::isValidId($params[0])) {
            return $this->error(404);
        }

        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');
        $product = Product::getProductById($id);
        if(!$product) {
            return $this->error(404);
        }

        if($yes) {
            $filePath = 'files/product/' . $product['photo'];
            if (is_file($filePath))
                unlink($filePath);
            Product::deleteProduct($id);
            $this->redirect('/product/index');
        }
        else {
            return $this->render(null, [
                'product' => $product
            ]);
        }

    }

    public function viewAction($params)
 {
        if (!Utils::isValidId($params[0])) {
            return $this->error('404');
        }
        $id = (int)$params[0];
        $product = Product::getProductById($id);
        if(!$product) {
            return $this->error('404');
        }
        return $this->render(null, [
            'product' => $product
        ]);
    }

}