<?php

namespace core\validation;


use Exception;



class Validation
{
    use FilterTrait;
    protected array $messages = [
        'name' => 'Поле %s не існує.',
        'type' => 'Поле %s має бути %s.',
        'required' => 'Поле %s є обов’язковим.',
        'pattern' => 'Поле %s має відповідати регулярному виразу %s.',
        'equal' => 'Поле %s має бути %s.',
        'length' => 'Поле %s не може бути менше %s і більше %s.',
        'maxSize' => 'Поле %s не може перевищувати %s.',
        'accept' => 'Поле %s має бути %s.',
    ];

    private string $name;
    private $value = null;
    private array $errors = [];
    private array $data;
    private  $file;

    public function name(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function value($value): self
    {

        $this->value = $value;
        return $this;

    }
    public function file($value){

        $this->file = $value;
        return $this;

    }

    protected function error(string $type, array $vars = [])
    {
        $this->errors[$this->name][] = vsprintf($this->messages[$type], $vars);
    }
    /**
     * Check if field value respects the type
     * and set data array.
     *
     * @param string $name
     * @return self
     * @throws InvalidFieldTypeException
     */
    public function type(string $type): self
    {
        if (!method_exists($this, $type)) {
            throw new Exception("The field type must be text, email, file, array, datetime, number, int, float, url, boolean or any");
        }

        if (!empty($this->value)) {
            if (!$this->{$type}($this->value)) {
                $this->error('type', [$this->name, $type]);
            }
        }

        $this->data[$this->name] = $this->sanitize($type, $this->value);
        if($this->file) {
            $this->data[$this->name] = $this->sanitize($type, $this->file);
        }
        return $this;
    }
    /**
     * Set field required.
     *
     * @return self
     */
    public function required(): self
    {
        $required = false;

        // array
        if (is_array($this->value) && empty($this->value)) {
            $required = true;
        }

        // file
        if((isset($this->file) && $this->file['error'] == 4)){
            $required = true;
        }

        // any
        if ($this->value === '' || $this->value === null) {
            $required = true;
        }

        if ($required) {
            $this->error('required', [$this->name]);
        }
        return $this;
    }
    /**
     * Check if field value matches the value.
     *
     * @param mixed $value
     * @return self
     */
    public function equal($value): self
    {
        if ($this->value != $value){
            $this->error('equal', [$this->name, $value]);
        }
        return $this;
    }
    /**
     * Set field length.
     *
     * @param int $min
     * @param int $max
     * @return self
     */
    public function length(int $min = null, int $max = null): self
    {
        $length = false;
        if(is_numeric($this->value)) {
            $value = $this->value + 0;
        }
        else {
            $value = strlen($this->value);
        }
        if(!is_null($min)) {
            if($value < $min) {
                $length = true;
            }
        }
        if(!is_null($max)) {
            if($value > $max) {
                $length = true;
            }
        }
        if ($length) {
            $this->error('length', [$this->name, $min, $max]);
        }
        return $this;
    }

    public function maxSize($size): self{

        if($this->file['error'] != 4 && $this->file['size'] > $size){
            $megabytes = number_format($size / 1048576, 2) .' MB';
            $this->error('maxSize', [$this->name, $megabytes]);
        }
        return $this;

    }
    public function ext(array $extensions): self{
        if($this->file['error'] != 4 && !in_array(pathinfo($this->file['name'], PATHINFO_EXTENSION), $extensions) && !in_array(strtoupper(pathinfo($this->file['name'], PATHINFO_EXTENSION)), $extensions)){
            $this->error('accept', [$this->name, implode(', ', $extensions)]);
        }
        return $this;

    }
    public function isSuccess(): bool
    {
        return empty($this->errors);
    }
    public function getErrors(): array
    {
        return $this->errors;
    }
    public function getDisplayErrors() {
        $r = [];
        foreach ($this->errors as $k => $v) {
           $r[$k] = implode(';', $v);
        }
        return  $r;
    }
    public function result(): array
    {
        return [
            'status'    => empty($this->errors),
            'data'      => $this->getData(),
            'errors'    => $this->getErrors(),
        ];
    }
    public function getData(string $param = null)
    {
        if (!$param) {
            return $this->data;
        }
        return $this->data[$param] ?? NULL;
    }
    public function pattern(string $regex): self
    {
        $pattern     = '/^('.$regex.')$/u';

        if(!preg_match($pattern, $this->value)){
            $this->error('pattern', [$this->name, $regex]);
        }

        $this->data[$this->name] = $this->value;
        return $this;
    }
}