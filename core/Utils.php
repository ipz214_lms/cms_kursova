<?php

namespace core;

class Utils
{
    public static function filterArray($array, $fieldsList)
    {
        $newArray = [];
        foreach ($array as $key => $value)
            if (in_array($key, $fieldsList))
                $newArray[$key] = $value;
        return $newArray;
    }

    /**
     * Використовується для перевірки id з params
     * @param $id
     * @return bool
     */
    public static function isValidId($id) {
        if(is_numeric($id)) {
            return (int)$id > 0;
        }
        else
            return false;
    }
    public static function uniqFileName($path, $format) {
        do {
            $fileName = uniqid() . $format;
            $newPath = $path . $fileName;
        } while (file_exists($newPath));
        return $newPath;
    }
}