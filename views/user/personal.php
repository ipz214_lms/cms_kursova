<?php /** @var TYPE_NAME $user */
/** @var array $errors */
$user = $model ?? $user;
?>

<div class="tab-pane fade show active" id="account" role="tabpanel" aria-labelledby="account-tab">
    <h3 class="mb-4">Особиста інформація</h3>
    <form method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Ім'я</label>
                    <input type="text" name="first_name" class="form-control" value="<?=$user['first_name']?>">
                    <p class="text-danger"><?= $errors['first_name'] ?? "" ?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Прізвище</label>
                    <input type="text" name="last_name" class="form-control" value="<?=$user['last_name']?>">
                    <p class="text-danger"><?= $errors['last_name'] ?? "" ?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Номер телефону</label>
                    <input type="text" name="phone" class="form-control" value="<?=$user['phone']?>">
                    <p class="text-danger"><?= $errors['phone'] ?? "" ?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Адреса</label>
                    <input type="text" name="address" class="form-control" value="<?=$user['address']?>" >
                    <p class="text-danger"><?= $errors['address'] ?? "" ?></p>
                </div>
            </div>

        </div>
        <div>
    </form>
    <button class="btn btn-primary">Update</button>
    <button class="btn btn-light">Cancel</button>
</div>
</div>