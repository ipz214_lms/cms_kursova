<?php
/** @var string $content */
$content = empty($content) ? "" : $content;
?>
    <h1 class="mb-5">Кабінет</h1>
    <div class="bg-white shadow rounded-lg d-block d-sm-flex">
        <div class="profile-tab-nav border-right">
            <div class="p-4">

                <h4 class="text-center"><?= $user['first_name'] .' ' . $user['last_name']?></h4>
            </div>
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link " id="account-tab" data-toggle="pill" href="/user/personal" role="tab" aria-controls="account" aria-selected="false">
                    <i class="fa fa-home text-center mr-1"></i>
                    Особиста інформація
                </a>
            </div>
        </div>
        <div class="tab-content p-4 p-md-5" id="v-pills-tabContent">
            <?= $content ?? "" ?>


        </div>
    </div>
