<?php
/** @var array $errors */
/** @var string $error */
?>
<div class="modal modal-signin position-static d-block" tabindex="-1" role="dialog" id="modalSignin">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2">Вхід в особистий кабінет</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-5 pt-0">
                <form method="post" action="/user/login">
                    <div class="form-floating mb-3">
                        <input type="text" name="email" class="form-control rounded-3" id="floatingInput"
                               placeholder="name@example.com">
                        <label for="floatingInput">Електронна адреса</label>
                        <p class="text-danger">
                            <?= $errors['email'] ?? "" ?>
                        </p>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="password" name="password" class="form-control rounded-3" id="floatingPassword"
                               placeholder="Password">
                        <label for="floatingPassword">Пароль</label>
                        <p class="text-danger">
                            <?= $errors['password'] ?? "" ?>
                        </p>
                    </div>
                    <p class="text-danger"><?= $error ?? "" ?></p>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-primary" type="submit">Увійти</button>
                    <hr class="my-4">
                    <a class="w-100 mb-2 btn btn-lg rounded-3 btn-success" href="/user/register">Створити обліковий
                        запис</a>
                </form>
            </div>
        </div>
    </div>
</div>
