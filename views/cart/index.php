<?php
/** @var array $products */
$products;

?>
<?php if(!empty($products)) :?>
    <a class="btn btn-primary" href="order/add">Оформити Замовлення</a>
        <div class="col">
<?php endif;?>
<table class="table-responsive my-2 bg-light table-bordered   w-100 text-center">
    <thead class="table-warning">
    <tr class="">
        <th scope="col">Назва</th>
        <th scope="col">Ціна</th>
        <th scope="col">Кількість</th>
    </tr>
    </thead>
    <tbody>
    <?php  foreach ($products as $product) : ?>
    <tr>
        <th scope="row"><?= $product['name'] ?></th>
        <td><?= $product['price'] ?></td>
        <td><?= $product['count'] ?></td>
    </tr>
    <tr >
        <td class="text-end"  colspan="3">
            <a class="text-danger" href="/cart/delete/<?= $product['product_id']?>">Видалити</a>
        </td>
    <?php endforeach; ?>

    </tbody>
</table>
</div>
