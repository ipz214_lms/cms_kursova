<?php
/** @var array $products */
/** @var bool $user */
?>

<h2>Обране</h2>

<div
        class="row gx-4 gx-lg-5 row-cols-1 row-cols-md-3 row-cols-xl-4 justify-content-center"
>
    <?php foreach ($products as $product) : ?>
        <div class="col mb-5">
            <div class="card h-100">
                <!-- Product image-->
                <img
                        class="card-img-top"
                        src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg"
                        alt="..."
                />
                <!-- Product details-->
                <div class="card-body p-4">
                    <div class="text-center">
                        <!-- Product name-->
                        <h5 class="fw-bolder"><?= $product['name'] ?></h5>
                        <!-- Product price-->
                        $40.00 - $80.00
                    </div>
                </div>
                <!-- Product actions-->
                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div class="text-center">
                        <a class="btn btn-outline-dark mt-auto" href="/cart/add/<?= $product['product_id'] ?>"
                        >Корзина</a
                        >
                    </div>
                    <div class="text-center">
                        <a class="btn btn-outline-dark mt-auto" href="/wish/remove/<?= $product['product_id'] ?>"
                        >Видалити</a
                        >
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
