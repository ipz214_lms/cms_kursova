<?php
/** @var array $category */

/** @var array[][] $products */

use models\User;

?>

<?php if (User::isAdmin()) : ?>
    <div class="mb-3">
        <a href="/product/add/<?= $category['category_id'] ?>" class="btn btn-success">Додати товар</a>
    </div>
<?php endif;?>


    <h1><?= $category['name'] ?></h1>
    <h2>Список товарів</h2>
    <div
            class="row gx-4 gx-lg-5 row-cols-1 row-cols-md-3 row-cols-xl-4 justify-content-center"
    >
        <?php foreach ($products as $product) :?>
        <div class="col mb-5">
            <div class="card h-100">
                <!-- Product image-->
                <?php $filePath = 'files/product/' . $product['image'];?>
                <?php if (is_file($filePath)) : ?>
                    <img src="/<?= $filePath ?>" class="card-img-top" style="
    aspect-ratio: 450 / 300;
" alt="">
                <?php else: ?>
                    <img src="/static/images/no-image.jpg" class="card-img-top" alt="">
                <?php endif; ?>

                <!-- Product details-->
                <div class="card-body p-4">
                    <div class="text-center">
                        <!-- Product name-->
                        <h5 class="fw-bolder"><?= $product['name']?></h5>
                        <!-- Product price-->
                        $40.00 - $80.00
                    </div>
                </div>
                <!-- Product actions-->
                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div class="text-center">
                        <a class="btn btn-outline-dark mt-auto" href="/cart/add/<?= $product['product_id'] ?>"
                        >Корзина</a
                        >
                    </div>
                    <div class="text-center">
                        <a class="btn btn-outline-dark mt-auto" href="/wish/add/<?= $product['product_id'] ?>"
                        >Обране</a
                        >
                    </div>
                    <div class="text-center">
                        <a class="btn btn-outline-dark mt-auto btn-success" href="/product/view/<?= $product['product_id'] ?>"
                        >Переглянути</a
                        >
                    </div>
                    <?php if (User::isAdmin()) : ?>
                        <div class="text-center">
                            <a class="btn btn-outline-dark mt-auto btn-danger" href="/product/delete/<?= $product['product_id'] ?>"
                            >Видалити</a
                            >
                        </div>
                        <div class="text-center">
                            <a class="btn btn-outline-dark mt-auto btn-success" href="/product/edit/<?= $product['product_id'] ?>"
                            >Змінити</a
                            >
                        </div>

                    <?php endif;?>

                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>

