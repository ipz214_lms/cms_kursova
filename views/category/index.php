<?php
/** @var array $rows */
  ?>

<h2>Каталог</h2>
<div class="row gx-4 gx-lg-5 row-cols-1 row-cols-md-3 row-cols-xl-4 justify-content-center">
    <?php foreach ($rows as $row) : ?>

        <div class="col mb-5">
            <div class="card h-100">

                <!-- Product image-->
                <?php $filePath = 'files/category/' . $row['image'];?>
                <?php if (is_file($filePath)) : ?>
                    <img src="/<?= $filePath ?>" class="card-img-top" style="
    aspect-ratio: 450 / 300;
" alt="">
                <?php else: ?>
                    <img src="/static/images/no-image.jpg" class="card-img-top" alt="">
                <?php endif; ?>
                <!-- Product details-->
                <div class="card-body p-4">
                    <div class="text-center">
                        <!-- Cat name-->
                        <h5 class="card-title"><a href="/category/view/<?= $row['category_id'] ?>"><?= $row['name'] ?></a></h5>
                    </div>
                </div>


            </div>
        </div>
    <?php endforeach; ?>
</div>



