<?php
/** @var array $rows */
use models\User;
?>
<h2>Список категорій</h2>
<?php if (User::isAdmin()) : ?>
<div class="mb-3">
    <a href="/category/add" class="btn btn-success">Додати категорію</a>
</div>
<?php endif; ?>

<div class="row gx-4 gx-lg-5 row-cols-1 row-cols-md-3 row-cols-xl-4 justify-content-center">
    <?php foreach ($rows as $row) : ?>

        <div class="col mb-5">
            <div class="card h-100">

                <!-- Product image-->
                <?php $filePath = 'files/category/' . $row['image'];?>
                <?php if (is_file($filePath)) : ?>
                    <img src="/<?= $filePath ?>" class="card-img-top" style="
    aspect-ratio: 450 / 300;
" alt="">
                <?php else: ?>
                    <img src="/static/images/no-image.jpg" class="card-img-top" alt="">
                <?php endif; ?>
                <!-- Product details-->
                <div class="card-body p-4">
                    <div class="text-center">
                        <!-- Cat name-->
                        <h5 class="card-title"><a href="/category/view/<?= $row['category_id'] ?>"><?= $row['name'] ?></a></h5>
                    </div>
                </div>
                <div class="card-body">
                    <a href="/category/edit/<?= $row['category_id'] ?>" class="btn btn-primary">Редагувати</a>
                    <a href="/category/delete/<?= $row['category_id'] ?>" class="btn btn-danger">Видалити</a>
                </div>

            </div>
        </div>
    <?php endforeach; ?>
</div>

