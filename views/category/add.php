<?php
/** @var array $errors */
/** @var array $model */
?>
<h2>Додавання категорії</h2>

<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва категорії</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="" value = "<?= $model['name'] ?? ''?>">
        <p class="text-danger"><?= $errors['name'] ?? "" ?></p>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Файл з фотографією для категорії</label>
        <input type="file" class="form-control" name="file" id="file" accept=".png, .jpg" />
        <p class="text-danger"><?= $errors['file'] ?? ''?></p>
    </div>
    <div>
        <button class="btn btn-primary">Додати</button>
    </div>
</form>