<?php
/** @var array $errors */
/** @var array $model */
/** @var array $categories */
/** @var int $category_id */
var_dump($errors);
var_dump($model);
?>
<h2>Додавання товару</h2>

<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва товару</label>
        <input type="text" required minlength="3" maxlength="50" class="form-control" id="name" name="name"
               placeholder="" value="<?= $model['name'] ?? '' ?>">
        <p class="text-danger"><?= $errors['name'] ?? "" ?></p>
    </div>
    <div class="mb-3">
        <label for="category_id" class="form-label">Категорія товару</label>
        <select class="form-control" id="category_id" name="category_id" placeholder="Опис товару">
            <?php foreach ($categories as $category) : ?>

                <option <?= $category_id == $category['category_id'] ? 'selected' : '' ?>
                        value="<?= $category['category_id'] ?>"><?= $category['name'] ?></option>
            <?php endforeach; ?>
        </select>
        <p class="text-danger"><?= $errors['description'] ?? "" ?></p>
    </div>
    <div class="mb-3">
        <label for="count" class="form-label">На складі</label>
        <input type="text" required class="form-control" id="count" name="count" min="0"
               value="<?= $model['quantity'] ?>">
        <p class="text-danger"></p>
    </div>
    <div class="mb-3">
        <label for="price" class="form-label">Ціна</label>
        <input type="text" required class="form-control" id="price" name="price" min="0" step="any"
               value="<?= $model['price'] ?>">
        <p class="text-danger"></p>
    </div>
    <div class="mb-3">
        <label for="description" class="form-label">Опис товару</label>
        <textarea class="form-control editor" id="description" name="description"
                  placeholder="Опис товару"><?= $model['description'] ?></textarea>
        <p class="text-danger"><?= $errors['description'] ?? "" ?></p>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Файл з фотографією для товару</label>
        <input type="file" class="form-control" name="file" id="file" accept=".png, .jpg"/>
        <p class="text-danger"><?= $errors['file'] ?? '' ?></p>
    </div>
    <div>
        <button class="btn btn-primary" formnovalidate>Додати</button>
    </div>
</form>
<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('.editor'))
        .catch(error => {
            console.error(error);
        });
</script>


