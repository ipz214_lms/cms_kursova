<?php
/** @var array $product */

?>
    <div class="row d-flex justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="row">
                    <div class="col-md-6">
                        <div class="images p-3">
                            <?php $filePath = 'files/product/' . $product['image'];?>
                            <?php if (is_file($filePath)) : ?>
                                <img src="/<?= $filePath ?>" class="card-img-top" style="
    aspect-ratio: 450 / 300;
" alt="">
                            <?php else: ?>
                                <img src="/static/images/no-image.jpg" class="card-img-top" alt="">
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="product p-4">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center"> <i class="fa fa-long-arrow-left"></i> <span class="ml-1">Back</span> </div> <i class="fa fa-shopping-cart text-muted"></i>
                            </div>
                            <div class="mt-4 mb-3"> <span class="text-uppercase text-muted brand"></span>
                                <h5 class="text-uppercase"><?= $product['name'] ?? ''?></h5>
                                <div class="price d-flex flex-row align-items-center"> <span class="act-price"><?= $product['price'] ?? ''?> $</span>
                                    <div class="ml-2"> <small class="dis-price"></small> <span></span> </div>
                                </div>
                            </div>
                            <div class="mt-4 mb-3">
                                <span>На складі: <?= $product['count'] ?? ''?></span>
                            </div>
                            <form method="post"></form>
                            <div class="mb-3">
                                <label for="count" class="form-label">Кількість</label>
                                <input type="text" required class="form-control" id="count" name="count" min="0">
                            </div>
                            <div>
                                <button class="btn btn-primary">В кошик</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <h3>Опис товару</h3>
                    <div class="col"><?=$product['description']?></div>
                </div>
            </div>
        </div>
    </div>
