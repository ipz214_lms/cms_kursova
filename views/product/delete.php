<?php
/** @var array $product */
?>
<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Видалити товар "<?=$product['name'] ?>"?</h4>

    <p class="mb-0">
        <a href="/product/delete/<?=$product['product_id'] ?>/yes" class="btn btn-danger">Видалити</a>
        <a href="/product" class="btn btn-light">Відмінити</a>
    </p>
</div>
