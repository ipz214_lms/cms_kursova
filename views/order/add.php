<?php
/** @var array $errors */
/** @var array $model */

?>
<h2>Оформлення замовлення</h2>

<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="firstname" class="form-label">Ім'я</label>
        <input type="text" class="form-control" id="firstname" name="first_name" placeholder="" value = "<?= $model['first_name'] ?? ''?>">
        <p class="text-danger"><?= $errors['first_name'] ?? "" ?></p>
    </div>
    <div class="mb-3">
        <label for="lasttname" class="form-label">Прізвище</label>
        <input type="text" class="form-control" id="lasttname" name="last_name" placeholder="" value = "<?= $model['last_name'] ?? ''?>">
        <p class="text-danger"><?= $errors['last_name'] ?? "" ?></p>
    </div>
    <div class="mb-3">
        <label for="phone" class="form-label">Телефон</label>
        <input type="text" class="form-control" id="phone" name="phone" placeholder="" value = "<?= $model['phone'] ?? ''?>">
        <p class="text-danger"><?= $errors['phone'] ?? "" ?></p>
    </div>
    <div class="mb-3">
        <label for="address" class="form-label">Адреса</label>
        <input type="text" class="form-control" id="address" name="address" placeholder="" value = "<?= $model['address'] ?? ''?>">
        <p class="text-danger"><?= $errors['address'] ?? "" ?></p>
    </div>
    <div>
        <button class="btn btn-primary">ПІДТВЕРДИТИ ЗАМОВЛЕННЯ</button>
    </div>
</form>