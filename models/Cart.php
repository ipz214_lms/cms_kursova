<?php

namespace models;

use core\Utils;

class Cart
{
    public static function addItem($id, $count = 1) {
        if(!is_array($_SESSION['cart'])) {
            $_SESSION['cart'] = [];
        }
        $_SESSION['cart'][$id] += $count;
    }
    public static function getItems() {
        if(!is_array($_SESSION['cart'])) {
            return null;
        }
        $result = [];
        foreach ($_SESSION['cart'] as $id => $count) {
            $product= Product::getProductById($id);
            $product['count'] = $count;
            $result[] = $product;
        }
        return $result;
    }
    public static function deleteItem($id) {
        unset($_SESSION['cart'][$id]);
    }
}