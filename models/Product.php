<?php

namespace models;

use core\Core;
use core\Utils;

class Product
{
    protected static $tableName = 'products';
    protected static $columns = ['category_id', 'name', 'description', 'count', 'price'];
    public static function addProduct($row, $photoPath) {
        $fields = self::$columns;
        $row = Utils::filterArray($row, $fields);
        do {
            $fileName = uniqid() . '.jpg';
            $newPath = "files/product/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($photoPath, $newPath);
        $row['image'] = $fileName;
        Core::getInstance()->db->insert(self::$tableName, $row);
    }
    public static function deleteProduct($id) {
        Core::getInstance()->db->delete(self::$tableName, [
            'product_id' => $id
        ]);
    }
    public static function changePhoto($id, $newPhoto)
    {
        $row = self::getProductById($id);
        $photoPath = 'files/product/' . $row['image'];
        if (is_file($photoPath))
            unlink($photoPath);
        do {
            $fileName = uniqid() . '.jpg';
            $newPath = "files/product/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($newPhoto, $newPath);
        Core::getInstance()->db->update(self::$tableName, [
            'image' => $fileName
        ], [
            'product_id' => $id
        ]);
    }
    public static function updateProduct($id, $row) {
        $fields = self::$columns;
        $row = Utils::filterArray($row, $fields);
        Core::getInstance()->db->update(self::$tableName, $row, [
            'product_id' => $id
        ]);
    }
    public static function getProductById($id) {
        $row = Core::getInstance()->db->select(self::$tableName, '*', [
            'product_id' => $id
        ]);
        if (!empty($row)) {
            return $row[0];
        }
        else
            return null;
    }
    public static function getProductsByCategoryId($id) {
        return Core::getInstance()->db->select(self::$tableName, '*', [
            'category_id' => $id
        ]);
    }
}