<?php

namespace models;

use core\Core;
use core\Utils;

class Customer
{
    protected static $tableName = 'customers';

    public static function addCustomer($row) {

        $fields = ['first_name' , 'last_name' , 'phone' , 'address' ];
        $row = Utils::filterArray($row, $fields);

        var_dump($row);
        return Core::getInstance()->db->insert(self::$tableName, $row);
    }
}