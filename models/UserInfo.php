<?php

namespace models;

use core\Core;
use core\Utils;

class UserInfo
{
    protected static $tableName = 'user_info';
    public static function setUserInfo($row) {
        $fields = ['first_name', 'last_name', 'phone', 'address', 'photo'];
        $row = Utils::filterArray($row, $fields);
        return $userInfo = Core::getInstance()->db->insert(self::$tableName, $row);
    }
    public static function getUserInfo($id) {
        return Core::getInstance()->db->select(self::$tableName, '*', [
            'user_info_id' => $id
        ]);

    }
}