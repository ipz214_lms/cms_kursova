<?php

namespace models;

use core\Core;

class Wish
{
    protected static $tableName = 'wishes';
    // Session
//    public static function addSession($id) {
//        if(!is_array($_SESSION['wish'])) {
//            $_SESSION['wish'] = [];
//        }
//        $_SESSION['wish'][$id] = "";
//    }
//    public static function getItemsSession() {
//        if(!is_array($_SESSION['wish'])) {
//            return null;
//        }
//        $result = [];
//        foreach ($_SESSION['wish'] as $id => $nothing) {
//            $product= Product::getProductById($id);
//            $result[] = $product;
//        }
//        return $result;
//    }
//    public static function removeSession($id) {
//        unset($_SESSION['wish'][$id]);
//    }
//    public static function clearSession() {
//        unset($_SESSION['wish']);
//    }
    // DB
    public static function addDB($user_id, $id) {
        Core::getInstance()->db->insert(self::$tableName, [
            'user_id' => $user_id,
            'product_id' => $id
        ]);
    }
    public static function removeDB($user_id, $id) {
        Core::getInstance()->db->delete(self::$tableName, [
            'user_id' => $user_id,
            'product_id' => $id
        ]);
    }
    public static function getItemsDB($user_id) {
        $rows = Core::getInstance()->db->select(self::$tableName, 'product_id', [
            'user_id' => $user_id
        ]);
        if (empty($rows))
            return null;
        $result = [];

        foreach ($rows as $product) {
            $product= Product::getProductById($product['product_id']);
            $result[] = $product;
        }
        return $result;
    }
//    public static function saveDB($user_id) {
//        $products = self::getItemsSession() ?? [];
//
//        foreach ($products as $product) {
//            self::addDB($user_id, $product['product_id']);
//        }
//        self::clearSession();
//    }
}