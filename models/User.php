<?php

namespace models;

use core\Core;
use core\Utils;

class User
{
    protected static $tableName = 'users';


    public static function setUser($email, $password)
    {
        \core\Core::getInstance()->db->insert(
            self::$tableName, [
                'email' => $email,
                'password' => password_hash($password, PASSWORD_DEFAULT)
            ]
        );
    }

    public static function authenticateUser($email, $password) {
        $user = self::getUserByEmail($email);
        if(!empty($user)) {
            if (password_verify($password, $user['password'])) {
                return $user;
            }
        }
        return false;
    }
    public static function isEmailExists($email): bool
    {
        return !empty(static::getUserByEmail($email));
    }
    public static function getUserByEmail($email) {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'email' => $email
        ]);
        if (!empty($user))
            return $user[0];
        return null;
    }
    public static function addInfo($row) {
        $userId = self::getCurrentAuthorizedUser();
        $userId = $userId['user_id'] ?? null;
        $user_info_id = UserInfo::setUserInfo($row);
        if(!$userId || !$user_info_id)
            return false;

        Core::getInstance()->db->update(self::$tableName, [
            'user_info_id' => $user_info_id
        ], [
            'user_id' => $userId
        ]);
    }
    public static function getCurrentUserId() {
        $userId = self::getCurrentAuthorizedUser();
        $userId = $userId['user_id'] ?? false;
        return $userId;
    }
    public static function getInfo() {
       $userId = self::getCurrentUserId();
        if(!$userId)
            return false;
        $infoId = Core::getInstance()->db->select(self::$tableName, 'user_info_id', [
            'user_id' => $userId
        ]);
        if(!$infoId)
            return false;
        $infoId = $infoId[0]['user_info_id'];
       $row = UserInfo::getUserInfo($infoId);
        if (!empty($row)) {
            return $row[0];
        }
        else
            return null;
    }
    public static function authorizeUser($user) {
        $_SESSION['user'] = $user;
    }
    public static function logoutUser() {
        unset($_SESSION['user']);
    }
    public static function isUserAuthorize() {
        return isset($_SESSION['user']);
    }
    public static function getCurrentAuthorizedUser() {
        return $_SESSION['user'];
    }

    public static function isAdmin() {
        $user = self::getCurrentAuthorizedUser();
        return $user['access_level'] == 10;
    }
}