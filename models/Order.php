<?php

namespace models;

use core\Core;
use core\Utils;

class Order
{
    protected static $tableName = 'orders';
    protected static $columns = ['customer_id ', 'total'];
    public static function addOrder($row) {
        $fields = ['customer_id'];
        $row = Utils::filterArray($row, $fields);
        return Core::getInstance()->db->insert(self::$tableName, $row);
    }
    public static function makeOrder($customer_id) {
        $order_id = Order::addOrder(['customer_id' => $customer_id]);
        $products = Cart::getItems();
        self::addItems($order_id, $products);
    }
    public static function addItem($order_id, $product_id, $name, $count, $price) {
        Core::getInstance()->db->insert('order_lines', [
            'order_id' => $order_id,
            'product_id' => $product_id,
            'name' => $name,
            'quantity' => $count,
            'price' => $price]);
    }
    public static function addItems($order_id, $products) {
        foreach ($products as $product) {
            self::addItem($order_id, $product['product_id'], $product['name'], $product['count'], 1);
        }
    }
}