<?php
/** @var string $content */
/** @var string $title */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- Bootstrap CSS -->
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
    />
    <!-- Icons -->
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css"
    />
    <title><?= $title ?></title>
</head>
<body>
<header>
    <div class="superNav border-bottom py-2 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 centerOnMobile">
                    <select class="me-3 border-0 bg-light">
                        <option value="en-us">UA-US</option>
                    </select>
                    <span
                            class="d-none d-lg-inline-block d-md-inline-block d-sm-inline-block d-xs-none me-3"
                    ><strong>xeTech@gmail.com</strong></span
                    >
                    <span class="me-3"
                    ><i class="fa-solid fa-phone me-1 text-warning"></i>
                <strong>+380 934 24 68 81</strong></span
                    >
                </div>
                <div
                        class="col-lg-6 col-md-6 col-sm-12 col-xs-12 d-none d-lg-block d-md-block-d-sm-block d-xs-none text-end"
                >
              <span class="me-3"
              ><i class="fa-solid fa-truck text-muted me-1"></i
                  ><a class="text-muted" href="#">Shipping</a></span
              >
                    <span class="me-3"
                    ><i class="fa-solid fa-file text-muted me-2"></i
                        ><a class="text-muted" href="#">Policy</a></span
                    >
                </div>
            </div>
        </div>
    </div>
</header>
<nav
        class="navbar navbar-expand-lg bg-white sticky-top navbar-light p-3 shadow-sm"
>
    <div class="container">
        <a class="navbar-brand" href="#"
        ><i class="fa-solid fa-shop me-2"></i> <strong>ExeTech</strong></a
        >
        <button
                class="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown"
                aria-expanded="false"
                aria-label="Toggle navigation"
        >
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="mx-auto my-3 d-lg-none d-sm-block d-xs-block">
            <div class="input-group">
            <span class="border-warning input-group-text bg-warning text-white"
            ><i class="fa-solid fa-magnifying-glass"></i
                ></span>
                <input
                        type="text"
                        class="form-control border-warning"
                        style="color: #7a7a7a"
                />
                <button class="btn btn-warning text-white">Знайти</button>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <div class="ms-auto d-none d-lg-block">
                <div class="input-group">
              <span
                      class="border-warning input-group-text bg-warning text-white"
              ><i class="fa-solid fa-magnifying-glass"></i
                  ></span>
                    <input
                            type="text"
                            class="form-control border-warning"
                            style="color: #7a7a7a"
                    />
                    <button class="btn btn-warning text-white">Знайти</button>
                </div>
            </div>
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a
                            class="nav-link mx-2 text-uppercase active"
                            aria-current="page"
                            href="#"
                    >Головна</a
                    >
                </li>
                <li class="nav-item">
                    <a class="nav-link mx-2 text-uppercase" href="#">Offers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mx-2 text-uppercase" href="/category/index">Каталог</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mx-2 text-uppercase" href="#"></a>
                </li>
            </ul>
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link mx-2 text-uppercase" href="/wish">
                        <i class="bi bi-bookmark-heart-fill"></i
                        ></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link mx-2 text-uppercase" href="/cart"
                    ><i class="bi bi-basket3-fill"></i
                        ></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mx-2 text-uppercase" href="/user">Кабінет</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<main>
    <section class="py-5">
        <div class="container">
            <?= $content ?>
        </div>

    </section>
</main>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"
></script>
</body>
</html>
